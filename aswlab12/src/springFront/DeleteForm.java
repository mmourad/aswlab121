package springFront;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;



public class DeleteForm {

	@NotNull
	protected String[] tweetids;
	
	@NotEmpty
	protected String userpasswd;

	public String[] getTweetids() {
		return tweetids;
	}

	public void setTweetids(String[] tweetids) {
		this.tweetids = tweetids;
	}

	public String getUserpasswd() {
		return userpasswd;
	}

	public void setUserpasswd(String userpasswd) {
		this.userpasswd = userpasswd;
	}

	
		
}
