package springFront;

import javax.servlet.http.HttpServletRequest;

import net.tanesha.recaptcha.ReCaptcha;
import net.tanesha.recaptcha.ReCaptchaFactory;
import net.tanesha.recaptcha.ReCaptchaResponse;

import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

public class RegFormValidator implements Validator {
	
	private static String RECAPTCHA_PUBLICKEY = "6Lc_QgUAAAAAAFyWFhYONWN22eeUrgGoF5W7EZ3D";
	private static String RECAPTCHA_PRIVATEKEY = "6Lc_QgUAAAAAAOdDbdiPkpiguqVPhKlu5jK-ZSOd";
	
	private HttpServletRequest request;
	
	
	public RegFormValidator(HttpServletRequest request) {
		super();
		this.request = request;
	}

	@SuppressWarnings("rawtypes")
	public boolean supports(Class candidate) {
		return RegistrationForm.class.isAssignableFrom(candidate);
	}

	@Override
	public void validate(Object obj, Errors errors) {

		// Check for mandatory data
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "reg_username", "username.required", "Username is required.");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "reg_password", "password.required", "Password is required.");
		
		String recaptcha_challenge_field = ((RegistrationForm) obj).getRecaptcha_challenge_field();
		String recaptcha_response_field = ((RegistrationForm) obj).getRecaptcha_response_field();
		
		ReCaptcha captcha = ReCaptchaFactory.newReCaptcha(RECAPTCHA_PUBLICKEY, RECAPTCHA_PRIVATEKEY, false);
		ReCaptchaResponse res = captcha.checkAnswer(request.getRemoteAddr(), recaptcha_challenge_field, recaptcha_response_field);

		if (!res.isValid()) errors.rejectValue("recaptcha_response_field","captcha.incorrect", "Incorrect CAPTCHA input");

	}

}
